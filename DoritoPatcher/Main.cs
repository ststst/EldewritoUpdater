﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;

using Newtonsoft.Json.Linq;
using System.Threading;
using System.Net;

namespace DoritoPatcher
{
    using System.Diagnostics;

    public partial class Main : Form
    {
        SHA1 hasher = SHA1.Create();
        Dictionary<string, string> fileHashes;
        string[] skipFolders = { ".inn.meta.dir", ".inn.tmp.dir", "Frost", "tpi" };
        string[] skipFileExtensions = { ".bik" };
        string[] skipFiles = { "eldorado.exe", "tags.dat", "game.cfg", "font_package.bin" };

        JObject settingsJson;
        JObject updateJson;
        string latestUpdateVersion;
        JToken latestUpdate;

        List<string> filesToDownload;

        Thread validateThread;

        public string BasePath = @"H:\Games\HaloTest";

        public Main()
        {
            InitializeComponent();
            this.CenterToScreen();
            BasePath = Directory.GetCurrentDirectory();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            btnStartGame.SendToBack();
            btnStartGame.Parent = menuPanel;
            btnStartGame.Enabled = false;

            btnUpdate.SendToBack();
            btnUpdate.Parent = menuPanel;
            btnUpdate.Enabled = false;

            btnIRC.SendToBack();
            btnIRC.Parent = menuPanel;

            btnBug.SendToBack();
            btnBug.Parent = menuPanel;

            btnSettings.SendToBack();
            btnSettings.Parent = menuPanel;
            btnSettings.Enabled = false;

            btnExit.SendToBack();
            btnExit.Parent = menuPanel;

            try
            {
                settingsJson = JObject.Parse(File.ReadAllText("dewrito.json"));

                if (settingsJson["gameFiles"] == null || settingsJson["updateServiceUrl"] == null)
                {
                    SetStatus("Failed to read Dewrito updater configuration.", Color.Red);
                    return;
                }
            }
            catch
            {
                SetStatus("Failed to read Dewrito updater configuration.", Color.Red);
                return;
            }

           // CreateHashJson();
            validateThread = new Thread(new ThreadStart(BackgroundThread));
            validateThread.Start();
        }

        private void SetStatus(string status, Color color, bool updateLabel = true)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(() => SetStatus(status, color, updateLabel)));
                return;
            }
            if(updateLabel)
                lblStatus.Text = status;
            if (color == Color.Black)
                txtLog.AppendText(status + Environment.NewLine);
            else
                txtLog.AppendText(status + Environment.NewLine, color);
            //txtLog.Text = status + Environment.NewLine + txtLog.Text;
        }

        private void BackgroundThread()
        {
            if (!CompareHashesWithJson())
                return;

            SetStatus("Game files validated, contacting update server...", Color.Black);

            if(!ProcessUpdateData())
            {
                SetStatus("Failed to retrieve update information.", Color.Red);
                return;
            }

            if(filesToDownload.Count <= 0)
            {
                SetStatus("You have the latest version! (" + latestUpdateVersion + ")", Color.Green);
                btnStartGame.Enabled = true;
                return;
            }

            SetStatus("An update is available. (" + latestUpdateVersion + ")", Color.Green);
            BeginInvoke(new Action(() => { btnUpdate.Enabled = true; }));
        }

        private bool ProcessUpdateData()
        {
            try
            {
                string updateData = settingsJson["updateServiceUrl"].ToString().Replace("\"", "");
                if (updateData.StartsWith("http"))
                {
                    var wc = new WebClient();
                    try
                    {
                        updateData = wc.DownloadString(updateData);
                    }
                    catch
                    {
                        return false;
                    }
                }
                else
                {
                    if (!File.Exists(updateData))
                        return false;

                    updateData = File.ReadAllText(updateData);
                }

                updateJson = JObject.Parse(updateData);

                latestUpdate = null;
                foreach (var x in updateJson)
                {
                    if (x.Value["releaseNo"] == null || x.Value["gitRevision"] == null || 
                        x.Value["baseUrl"] == null || x.Value["files"] == null)
                    {
                        return false; // invalid update data
                    }

                    if (latestUpdate == null ||
                        int.Parse(x.Value["releaseNo"].ToString()) > int.Parse(latestUpdate["releaseNo"].ToString()))
                    {
                        latestUpdate = x.Value;
                        latestUpdateVersion = x.Key + "-" + latestUpdate["gitRevision"];
                    }
                }

                if (latestUpdate == null)
                    return false;

                List<string> patchFiles = new List<string>();
                foreach (var file in latestUpdate["patchFiles"]) // each file mentioned here must match original hash or have a file in the _dewbackup folder that does
                {
                    string fileName = (string)file;
                    string fileHash = (string)settingsJson["gameFiles"][fileName];
                    if (!fileHashes.ContainsKey(fileName) && !fileHashes.ContainsKey(Path.Combine("_dewbackup", fileName)))
                    {
                        SetStatus("Original file data for file \"" + fileName + "\" not found.", Color.Red);
                        SetStatus("Please redo your Halo Online installation with the original HO files.", Color.Red, false);
                        return false;
                    }

                    if (fileHashes.ContainsKey(fileName)) // we have the file
                    {
                        if (fileHashes[fileName] != fileHash &&
                            (!fileHashes.ContainsKey(Path.Combine("_dewbackup", fileName)) || fileHashes[Path.Combine("_dewbackup", fileName)] != fileHash))
                        {
                            SetStatus("File \"" + fileName + "\" was found but isn't original, and a valid backup of the original data wasn't found.", Color.Red);
                            SetStatus("Please redo your Halo Online installation with the original HO files.", Color.Red, false);
                            return false;
                        }
                    }
                    else
                    {
                        // we don't have the file
                        if (!fileHashes.ContainsKey(fileName + ".orig") &&
                            (!fileHashes.ContainsKey(Path.Combine("_dewbackup", fileName)) || fileHashes[Path.Combine("_dewbackup", fileName)] != fileHash))
                        {
                            SetStatus("Original file data for file \"" + fileName + "\" not found.", Color.Red);
                            SetStatus("Please redo your Halo Online installation with the original HO files.", Color.Red, false);
                            return false;
                        }
                    }

                    patchFiles.Add(fileName);
                }

                IDictionary<string, JToken> files = (JObject)latestUpdate["files"];

                filesToDownload = new List<string>();
                foreach (var x in files)
                {
                    string keyName = x.Key;
                    if (!fileHashes.ContainsKey(keyName) && fileHashes.ContainsKey(keyName.Replace(@"\", @"/"))) // linux system maybe?
                        keyName = keyName.Replace(@"\", @"/");

                    if (!fileHashes.ContainsKey(keyName) || fileHashes[keyName] != x.Value.ToString())
                    {
                        SetStatus("File \"" + keyName + "\" is missing or an older version.", Color.Red);
                        var name = x.Key;
                        if (patchFiles.Contains(keyName))
                            name += ".bspatch";
                        filesToDownload.Add(name);
                    }
                }

                return true;
            }
            catch (WebException)
            {
                return false;
            }
            catch (NullReferenceException)
            {
                return false;
            }
        }

        private bool CompareHashesWithJson()
        {
            if (fileHashes == null)
                HashFilesInFolder(BasePath);

            IDictionary<string, JToken> files = (JObject)settingsJson["gameFiles"];

            foreach (var x in files)
            {
                string keyName = x.Key;
                if (!fileHashes.ContainsKey(keyName) && fileHashes.ContainsKey(keyName.Replace(@"\", @"/"))) // linux system maybe?
                    keyName = keyName.Replace(@"\", @"/");

                if (!fileHashes.ContainsKey(keyName))
                {
                    if(skipFileExtensions.Contains(Path.GetExtension(keyName)))
                        continue;

                    SetStatus("Failed to find required game file \"" + x.Key + "\"", Color.Red);
                    SetStatus("Please redo your Halo Online installation with the original HO files.", Color.Red, false);
                    return false;
                }

                if (fileHashes[keyName] != x.Value.ToString().Replace("\"", ""))
                {
                    if (skipFileExtensions.Contains(Path.GetExtension(keyName)) || skipFiles.Contains(Path.GetFileName(keyName)))
                        continue;

                    SetStatus("Game file \"" + keyName + "\" data is invalid.", Color.Red);
                    SetStatus("Your hash: " + fileHashes[keyName], Color.Red, false);
                    SetStatus("Expected hash: " + x.Value.ToString().Replace("\"", ""), Color.Red, false);
                    SetStatus("Please redo your Halo Online installation with the original HO files.", Color.Red, false);
                    return false;
                }
            }

            return true;
        }

        private void CreateHashJson()
        {
            if(fileHashes == null)
                HashFilesInFolder(BasePath);

            StringBuilder builder = new StringBuilder();
            builder.AppendLine("{");
            foreach (var kvp in fileHashes)
            {
                builder.AppendLine(String.Format("    \"{0}\": \"{1}\",", kvp.Key.Replace(@"\", @"\\"), kvp.Value));
            }
            builder.AppendLine("}");
            var json = builder.ToString();
            json = json;
        }

        private void HashFilesInFolder(string basePath, string dirPath = "")
        {
            if (fileHashes == null)
                fileHashes = new Dictionary<string, string>();

            if (String.IsNullOrEmpty(dirPath))
            {
                dirPath = basePath;
                SetStatus("Validating game files...", Color.Black);
            }

            foreach (var folder in Directory.GetDirectories(dirPath))
            {
                var dirName = Path.GetFileName(folder);
                if (skipFolders.Contains(dirName))
                    continue;
                HashFilesInFolder(basePath, folder);
            }

            foreach (var file in Directory.GetFiles(dirPath))
            {
                try
                {
                    using (var stream = File.OpenRead(file))
                    {
                        var hash = hasher.ComputeHash(stream);
                        var hashStr = BitConverter.ToString(hash).Replace("-", "");

                        var fileKey = file.Replace(basePath, "");
                        if ((fileKey.StartsWith(@"\") || fileKey.StartsWith("/")) && fileKey.Length > 1)
                            fileKey = fileKey.Substring(1);
                        if (!fileHashes.ContainsKey(fileKey))
                            fileHashes.Add(fileKey, hashStr);
                    }
                }
                catch { }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btns_MouseHover(object sender, EventArgs e)
        {
            var button = (Button)sender;

            button.ForeColor = Color.Black;
        }

        private void btns_MouseLeave(object sender, EventArgs e)
        {
            var button = (Button)sender;

            button.ForeColor = Color.White;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            foreach (var file in filesToDownload)
            {
                SetStatus("Downloading file \"" + file + "\"...", Color.Black);
                var url = latestUpdate["baseUrl"].ToString().Replace("\"", "") + file;
                var destPath = Path.Combine(BasePath, file);
                FileDownloadDialog dialog = new FileDownloadDialog(this, url, destPath);
                var result = dialog.ShowDialog();
                if (result != DialogResult.OK)
                {
                    SetStatus("Download for file \"" + file + "\" failed.", Color.Red);
                    SetStatus("Error: " + dialog.Error.Message, Color.Red, false);
                    if (dialog.Error.InnerException != null)
                        SetStatus("Error: " + dialog.Error.InnerException.Message, Color.Red, false);
                    return;
                }
            }

            MessageBox.Show("Update complete! Run eldorado.exe to start the game, and enjoy!", "ElDewrito Updater");
            btnStartGame.Enabled = true;
        }

        private void btnIRC_Click(object sender, EventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("http://irc.lc/gamesurge/eldorito");
            Process.Start(sInfo);
        }

        private void btnBug_Click(object sender, EventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("https://gitlab.com/emoose/ElDorito/issues");
            Process.Start(sInfo);
        }

        private void btnStartGame_Click(object sender, EventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo(BasePath + "/eldorado.exe");

            try
            {
                Process.Start(sInfo);
            }
            catch
            {
                MessageBox.Show("Game executable not found.");
            }

        }
    }
}
