﻿using System;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Diagnostics;

namespace DoritoPatcher
{
    public partial class FileDownloadDialog : Form
    {
        WebClient wc = new WebClient();
        public Exception Error;
        public Main MainForm;

        public FileDownloadDialog(Main mainForm, string url, string destPath)
        {
            InitializeComponent();
            this.MainForm = mainForm;

            lblStatus.Text = "Downloading " + System.IO.Path.GetFileName(destPath);

            // sketchy code to replace current exe
            if(File.Exists(destPath) && destPath.ToLower() == Process.GetCurrentProcess().MainModule.FileName.ToLower())
            {
                if (File.Exists(destPath + ".old"))
                    File.Delete(destPath + ".old");

                File.Move(destPath, destPath + ".old");
            }

            wc.DownloadProgressChanged += (s, e) =>
            {
                progressBar1.Value = e.ProgressPercentage;
            };
            wc.DownloadFileCompleted += (s, e) =>
            {
                progressBar1.Value = 100;
                this.DialogResult = DialogResult.OK;
                if (e.Error != null)
                {
                    this.DialogResult = DialogResult.Abort;
                    Error = e.Error;
                }
                var patchFileExtension = ".bspatch";
                if (destPath.EndsWith(patchFileExtension))
                {
                    string destFilePath = destPath.Substring(0, destPath.Length - patchFileExtension.Length);
                    string destFileName = destFilePath.Replace(MainForm.BasePath, "");
                    if (destFileName.StartsWith("\\") || destFileName.StartsWith("/"))
                        destFileName = destFileName.Substring(1);

                    // patch file
                    string backupFolder = Path.Combine(MainForm.BasePath, "_dewbackup");
                    string backupFile = Path.Combine(backupFolder, destFileName);
                    if (File.Exists(backupFile))
                    {
                        // backup exists, copy backup over original file and patch orig
                        if (File.Exists(destFilePath))
                            File.Delete(destFilePath);

                        //File.Copy(backupFile, destFilePath);
                    }
                    else
                    {
                        // if backup don't exist we're assuming the main form made sure current one is fine
                        // b-b-b-b-back it up
                        backupFolder = Path.GetDirectoryName(backupFile);
                        if(!Directory.Exists(backupFolder))
                            Directory.CreateDirectory(backupFolder);

                        File.Copy(destFilePath, backupFile);
                        File.Delete(destFilePath);
                    }

                    lblStatus.Text = "Applying patch file...";
                    Application.DoEvents();

                    using (FileStream input = new FileStream(backupFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                    using (FileStream output = new FileStream(destFilePath, FileMode.Create))
                        BinaryPatchUtility.Apply(input, () => new FileStream(destPath, FileMode.Open, FileAccess.Read, FileShare.Read), output);

                    File.Delete(destPath);
                }

                this.Close();
            };
            wc.DownloadFileAsync(new Uri(url), destPath);
        }
    }
}
