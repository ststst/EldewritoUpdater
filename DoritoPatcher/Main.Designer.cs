﻿namespace DoritoPatcher
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtLog = new System.Windows.Forms.RichTextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.menuPanel = new System.Windows.Forms.PictureBox();
            this.btnStartGame = new System.Windows.Forms.Button();
            this.btnIRC = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnBug = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.Logo = new System.Windows.Forms.PictureBox();
            this.btnExit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.menuPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            this.SuspendLayout();
            // 
            // txtLog
            // 
            this.txtLog.BackColor = System.Drawing.Color.Black;
            this.txtLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLog.ForeColor = System.Drawing.Color.White;
            this.txtLog.Location = new System.Drawing.Point(340, 341);
            this.txtLog.Margin = new System.Windows.Forms.Padding(2);
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.Size = new System.Drawing.Size(433, 110);
            this.txtLog.TabIndex = 1;
            this.txtLog.Text = "";
            // 
            // lblStatus
            // 
            this.lblStatus.BackColor = System.Drawing.Color.Black;
            this.lblStatus.ForeColor = System.Drawing.Color.White;
            this.lblStatus.Location = new System.Drawing.Point(340, 293);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(433, 46);
            this.lblStatus.TabIndex = 4;
            this.lblStatus.Text = "Waiting...";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // menuPanel
            // 
            this.menuPanel.BackColor = System.Drawing.Color.Transparent;
            this.menuPanel.BackgroundImage = global::DoritoPatcher.Properties.Resources.panel;
            this.menuPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuPanel.Location = new System.Drawing.Point(12, 12);
            this.menuPanel.Name = "menuPanel";
            this.menuPanel.Size = new System.Drawing.Size(320, 380);
            this.menuPanel.TabIndex = 5;
            this.menuPanel.TabStop = false;
            // 
            // btnStartGame
            // 
            this.btnStartGame.BackColor = System.Drawing.Color.Transparent;
            this.btnStartGame.FlatAppearance.BorderSize = 0;
            this.btnStartGame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStartGame.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartGame.ForeColor = System.Drawing.Color.White;
            this.btnStartGame.Location = new System.Drawing.Point(12, 12);
            this.btnStartGame.Name = "btnStartGame";
            this.btnStartGame.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnStartGame.Size = new System.Drawing.Size(294, 50);
            this.btnStartGame.TabIndex = 6;
            this.btnStartGame.Text = "Start Game";
            this.btnStartGame.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStartGame.UseVisualStyleBackColor = false;
            this.btnStartGame.Click += new System.EventHandler(this.btnStartGame_Click);
            this.btnStartGame.MouseLeave += new System.EventHandler(this.btns_MouseLeave);
            this.btnStartGame.MouseHover += new System.EventHandler(this.btns_MouseHover);
            // 
            // btnIRC
            // 
            this.btnIRC.BackColor = System.Drawing.Color.Transparent;
            this.btnIRC.FlatAppearance.BorderSize = 0;
            this.btnIRC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIRC.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIRC.ForeColor = System.Drawing.Color.White;
            this.btnIRC.Location = new System.Drawing.Point(12, 124);
            this.btnIRC.Name = "btnIRC";
            this.btnIRC.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnIRC.Size = new System.Drawing.Size(294, 50);
            this.btnIRC.TabIndex = 7;
            this.btnIRC.Text = "Visit IRC";
            this.btnIRC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIRC.UseVisualStyleBackColor = false;
            this.btnIRC.Click += new System.EventHandler(this.btnIRC_Click);
            this.btnIRC.MouseLeave += new System.EventHandler(this.btns_MouseLeave);
            this.btnIRC.MouseHover += new System.EventHandler(this.btns_MouseHover);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.Transparent;
            this.btnUpdate.FlatAppearance.BorderSize = 0;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(12, 68);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnUpdate.Size = new System.Drawing.Size(294, 50);
            this.btnUpdate.TabIndex = 9;
            this.btnUpdate.Text = "Update Game";
            this.btnUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            this.btnUpdate.MouseLeave += new System.EventHandler(this.btns_MouseLeave);
            this.btnUpdate.MouseHover += new System.EventHandler(this.btns_MouseHover);
            // 
            // btnBug
            // 
            this.btnBug.BackColor = System.Drawing.Color.Transparent;
            this.btnBug.FlatAppearance.BorderSize = 0;
            this.btnBug.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBug.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBug.ForeColor = System.Drawing.Color.White;
            this.btnBug.Location = new System.Drawing.Point(12, 180);
            this.btnBug.Name = "btnBug";
            this.btnBug.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnBug.Size = new System.Drawing.Size(294, 50);
            this.btnBug.TabIndex = 8;
            this.btnBug.Text = "Report Bug";
            this.btnBug.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBug.UseVisualStyleBackColor = false;
            this.btnBug.Click += new System.EventHandler(this.btnBug_Click);
            this.btnBug.MouseLeave += new System.EventHandler(this.btns_MouseLeave);
            this.btnBug.MouseHover += new System.EventHandler(this.btns_MouseHover);
            // 
            // btnSettings
            // 
            this.btnSettings.BackColor = System.Drawing.Color.Transparent;
            this.btnSettings.FlatAppearance.BorderSize = 0;
            this.btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSettings.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSettings.ForeColor = System.Drawing.Color.White;
            this.btnSettings.Location = new System.Drawing.Point(12, 236);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnSettings.Size = new System.Drawing.Size(294, 50);
            this.btnSettings.TabIndex = 10;
            this.btnSettings.Text = "Settings";
            this.btnSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSettings.UseVisualStyleBackColor = false;
            this.btnSettings.MouseLeave += new System.EventHandler(this.btns_MouseLeave);
            this.btnSettings.MouseHover += new System.EventHandler(this.btns_MouseHover);
            // 
            // Logo
            // 
            this.Logo.BackColor = System.Drawing.Color.Transparent;
            this.Logo.BackgroundImage = global::DoritoPatcher.Properties.Resources.logo;
            this.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Logo.Location = new System.Drawing.Point(343, 12);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(399, 50);
            this.Logo.TabIndex = 11;
            this.Logo.TabStop = false;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(12, 293);
            this.btnExit.Name = "btnExit";
            this.btnExit.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnExit.Size = new System.Drawing.Size(294, 50);
            this.btnExit.TabIndex = 12;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            this.btnExit.MouseLeave += new System.EventHandler(this.btns_MouseLeave);
            this.btnExit.MouseHover += new System.EventHandler(this.btns_MouseHover);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::DoritoPatcher.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(784, 462);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.Logo);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnBug);
            this.Controls.Add(this.btnIRC);
            this.Controls.Add(this.btnStartGame);
            this.Controls.Add(this.menuPanel);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.txtLog);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ElDewrito Updater";
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.menuPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox txtLog;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.PictureBox menuPanel;
        private System.Windows.Forms.Button btnStartGame;
        private System.Windows.Forms.Button btnIRC;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnBug;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.PictureBox Logo;
        private System.Windows.Forms.Button btnExit;
    }
}

